CREATE TABLE Nummers(
Titel VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
Artiest VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
Genre VARCHAR(50),
Jaar CHAR(4));

CREATE TABLE Huisdieren(
Naam varchar(100) CHARACTER SET utf8mb4 NOT NULL,
Leeftijd SMALLINT NOT NULL,
Soort char(50) CHARACTER SET utf8mb4 NOT NULL);

RENAME TABLE Nummers TO Liedjes;

ALTER TABLE Liedjes
ADD Album Varchar(100) CHARACTER SET utf8mb4 NOT NULL;

ALTER TABLE Liedjes
DROP COLUMN Genre;

ALTER TABLE Huisdieren
ADD Baasje varchar(100) CHARACTER SET utf8mb4 NOT NULL;

CREATE TABLE IF NOT EXISTS Metingen(
Tijdstip DATETIME NOT NULL,
Grootte smallint,
CHECK (Grootte>0),
Marge DECIMAL(3,2)
);

INSERT INTO Liedjes(Titel, Artiest, Album, Jaar)
VALUES  ('John the Revelator', 'Larkin Poe', 'Peach', 2017),
        ("Missionary Man", "Ghost", "Popestar", 2016),
        ('Stairway to Heaven', 'Led Zeppelin', 'Led Zeppelin', 1971),
        ("Good Enough", "Molly Tuttle", "Rise", 2017),
        ("Outrage for the Execution of Willie McGee", "Goodnight, Texas", "Conductor", 2018),
        ("They Lie", "Layla Zoe", "The Lily", 2013),
        ("It Ain't You", "Danielle Nicole", "Wolf Den", 2015),
        ("Unchained", "Van Halen", "Fair Warning", 1981);


INSERT INTO Huisdieren(Baasje, Naam, Leeftijd, Soort)
VALUES  ('Vincent', 'Misty', 6, 'hond'),
        ('Christiane', 'Ming', 8, 'hond'),
        ('Esther', 'Bientje', 6, 'kat'),
        ('Jommeke', 'Flip', 75, 'papegaai'),
        ('Villads', 'Berto', 1, 'papegaai'),
        ('Bert', 'Ming', 7, 'kat'),
        ('Thaïs', 'Suerta', 2, 'hond'),
        ('Lyssa', 'Фёдор', 1, 'hond');

SELECT * FROM Liedjes;

SELECT Naam, Soort FROM Huisdieren;

SELECT Titel FROM Liedjes ORDER BY Jaar;

SELECT * FROM Huisdieren ORDER BY Naam, Soort;

SELECT concat(Naam,' de ', Soort) AS Omschrijving FROM Huisdieren; 

SELECT * FROM Liedjes Where Jaar LIKE '%1';