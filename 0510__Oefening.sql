USE ModernWays;
CREATE TABLE Nummers(
Titel VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Artiest VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Genre VARCHAR(50),
Jaar CHAR(4)
);

CREATE TABLE Huisdieren(
Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Leeftijd SMALLINT NOT NULL,
Soort char(50) CHAR SET utf8mb4 NOT NULL
);

RENAME TABLE Nummers TO Liedjes;

ALTER TABLE Liedjes
ADD Album VARCHAR(100) CHAR SET utf8mb4 NOT NULL;

ALTER TABLE Liedjes
DROP COLUMN Genre;

ALTER TABLE Huisdieren
ADD Baasje VARCHAR(100) CHAR SET utf8mb4 NOT NULL;

CREATE TABLE Metingen(
Tijdstip DATETIME NOT NULL,
Grootte SMALLINT,
CHECK (Grootte>0),
Marge DECIMAL(3,2) NOT NULL
);

INSERT INTO Liedjes(Titel, Artiest, Album, Jaar)
VALUES  ('John the Revelator', 'Larkin Poe', 'Peach', 2017),
        ("Missionary Man", "Ghost", "Popestar", 2016),
        ('Stairway to Heaven', 'Led Zeppelin', 'Led Zeppelin', 1971),
        ("Good Enough", "Molly Tuttle", "Rise", 2017),
        ("Outrage for the Execution of Willie McGee", "Goodnight, Texas", "Conductor", 2018),
        ("They Lie", "Layla Zoe", "The Lily", 2013),
        ("It Ain't You", "Danielle Nicole", "Wolf Den", 2015),
        ("Unchained", "Van Halen", "Fair Warning", 1981);
